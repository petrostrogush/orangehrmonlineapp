package com.orangehrmnline;

import org.openqa.selenium.WebDriver;


import com.orangehrmonline.tools.Browser;

import io.qameta.allure.Step;

public class OrangehrmonlineApp {

	private static final String LOGIN_PAGE_URL = "http://opensource.demo.orangehrmlive.com/";

	private WebDriver driver;

	public OrangehrmonlineApp() {
	}

	@Step("Open Login page by URL: " + LOGIN_PAGE_URL)
	public LoginPage openLoginPage() {
		driver = Browser.open();
		driver.get(LOGIN_PAGE_URL);
		return new LoginPage(driver);
	}

	@Step("Close application/browser")
	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

}
