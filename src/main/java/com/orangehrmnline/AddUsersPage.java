package com.orangehrmnline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.github.javafaker.Faker;

import io.qameta.allure.Step;

public class AddUsersPage {

	private static String username;

	@FindBy(css = "#systemUser_employeeName_empName")
	private WebElement employeeNameFld;

	@FindBy(css = "#systemUser_userName")
	private WebElement usernameFld;

	@FindBy(css = "#systemUser_password")
	private WebElement passwordFld;
	
	@FindBy(css = "#systemUser_confirmPassword")
	private WebElement confirmPasswordFld;
	
	@FindBy(css = "#btnSave")
	private WebElement saveBtn;

	private WebDriver driver;

	public AddUsersPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public static String generateUsername() {
		Faker faker = new Faker();
		String username = faker.name().username();
		return username;
	}
	
	@Step("Create a new user")
	public HomePage addNewUser(String username) {
		enterEmployeeName();
		enterUsername(username);
		createPassword();
		confirmPassword();
		clickSaveButton();
		return new HomePage(driver);
	}

	@Step("Enter Employee name")
	public void enterEmployeeName() {
		employeeNameFld.sendKeys("John Smith");
	}
	@Step("Enter username name")
	public void enterUsername(String username) {
		usernameFld.sendKeys(generateUsername());
	}
	@Step("Enter password")
	public void createPassword() {
		passwordFld.sendKeys("Qwerty12");
	}
	@Step("Confirm password")
	public void confirmPassword() {
		confirmPasswordFld.sendKeys("Qwerty12");
	}
	@Step("Click 'Save' button")
	public void clickSaveButton() {
		saveBtn.click();
		waitFor(2);
	}
	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
