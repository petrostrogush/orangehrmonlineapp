package com.orangehrmnline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import io.qameta.allure.Step;

public class LoginPage {

	@FindBy(css = "#txtUsername")
	private WebElement usernameField;

	@FindBy(css = "#txtPassword")
	private WebElement passwordField;

	@FindBy(css = "#btnLogin")
	private WebElement loginButton;

	private WebDriver driver;

	private String username;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	@Step("Login with as admin")
	public HomePage loginAsAdmin() {
		enterAdminName();
		enterAdminsPassword();
		clickLoginButton();
		return new HomePage(driver);
	}

	@Step("Enter admin name")
	public void enterAdminName() {
		usernameField.sendKeys("Admin");
	}

	@Step("Enter admin's password {password}")
	public void enterAdminsPassword() {
		passwordField.sendKeys("admin");
	}

	@Step("Click 'Login' button")
	public void clickLoginButton() {
		loginButton.click();
	}
	
	@Step("Login with as a user")
	public HomePage loginAsUser() {
		enterUsername();
		enterUsersPassword();
		clickLoginButton();
		return new HomePage(driver);
	}
	@Step("Enter username {username}")
	public void enterUsername() {
		usernameField.sendKeys(username);
	}
	@Step("Enter password {password}")
	public void enterUsersPassword() {
		usernameField.sendKeys("Qwerty12");
	}
	
}
