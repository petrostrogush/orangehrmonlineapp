package com.orangehrmnline;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

public class HomePage {

	@FindBy(css = "#menu_admin_viewAdminModule")
	private WebElement adminTab;

	@FindBy(css = "#menu_admin_UserManagement")
	private WebElement userManagementBtn;

	@FindBy(css = "#menu_admin_viewSystemUsers")
	private WebElement usersBtn;
	
	@FindBy(css = "#btnAdd")
	private WebElement addUserBtn;
	
	@FindBy(css = "#welcome")
	private WebElement welcomeBtn;
	
	@FindBy(xpath = "//a[contains(.,'Logout')]")
	private WebElement logoutBtn;

	private WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@Step("Navigate to 'Add user' page")
	public AddUsersPage goToAddUserPage() {
		clickAdminTab();
		clickUserManagementBtn();
		clickUsersBtn();
		clickAddUserBtn();
		return new AddUsersPage(driver);
	}
	
	@Step("Click on 'Admin' tab")
	public void clickAdminTab() {
		adminTab.click();
	}
	@Step("Choose 'UserManagement'")
	public void clickUserManagementBtn() {
		userManagementBtn.click();
	}
	@Step("Choose 'Users'")
	public void clickUsersBtn() {
		usersBtn.click();
	}
	@Step("Click 'Add' button")
	public AddUsersPage clickAddUserBtn() {
		addUserBtn.click();
		return new AddUsersPage(driver);
	}
	
	@Step("Logout from the app")
	public LoginPage logoutFromApp() {
		clickWelcomeBtn();
		clickLogoutBtn();
		return new LoginPage(driver);
	}
	@Step("Open the 'Welcome' menu")
	public void clickWelcomeBtn() {
		welcomeBtn.click();
		waitFor(2);
	}
	@Step("Click 'Logout'")
	public void clickLogoutBtn() {
		logoutBtn.click();
		waitFor(2);
	}
	@Step("Verify logged user")
	public String readUserName() {
		return welcomeBtn.getText();
	}
	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
