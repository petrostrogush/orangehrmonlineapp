package com.orangehrmonline.tools;

public interface WebApp {
	public byte[] takeScreenshot(String screenshotName);
}
