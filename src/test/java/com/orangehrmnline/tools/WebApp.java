package com.orangehrmnline.tools;

public interface WebApp {
	public byte[] takeScreenshot(String screenshotName);
}
