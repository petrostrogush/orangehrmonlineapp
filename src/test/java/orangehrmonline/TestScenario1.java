package orangehrmonline;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.orangehrmnline.AddUsersPage;
import com.orangehrmnline.HomePage;
import com.orangehrmnline.LoginPage;
import com.orangehrmnline.OrangehrmonlineApp;
import com.orangehrmnline.tools.WebApp;
import com.orangehrmnline.tools.WebAppTest;



public class TestScenario1 implements WebAppTest{
	
	OrangehrmonlineApp orangehrmonlineApp = new OrangehrmonlineApp();
	private LoginPage loginPage;
	private HomePage homePage;
	private AddUsersPage addUsersPage;
	
	private String username;
		
	@Test
	public void testScenario1() {
		loginPage = orangehrmonlineApp.openLoginPage();
		homePage = loginPage.loginAsAdmin();
		addUsersPage = homePage.goToAddUserPage();
		addUsersPage.addNewUser(username);
		homePage.logoutFromApp();
		loginPage.loginAsUser();
		
		String actualResult = homePage.readUserName();
		String expectedResult = "Welcome John";

		Assert.assertEquals(actualResult, expectedResult);
		
	}

	@Override
	public WebApp getTestedApp() {
		// TODO Auto-generated method stub
		return null;
	}

}
